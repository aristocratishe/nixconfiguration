# crosstool-ng.nix

{ lib, stdenv, fetchFromGitHub, autoconf, makeWrapper,
  which, automake, flex, gettext, coreutils, libtool, bison,
  texinfo, zlib, unzip, help2man, ncurses }:


let
  pkgs = import <nixpkgs> {};
in
pkgs.stdenv.mkDerivation rec {
  name = "crosstool-ng";

  meta = with lib; {
    description = "Crosstool-NG aims at building toolchains";
    license     = with licenses; [ gpl2Plus artistic2 ];
    platforms   = platforms.unix;
    maintainers = with maintainers; [ thoughtpolice ];
  };

  src = fetchFromGitHub {
    owner = "crosstool-ng";
    repo = "crosstool-ng";
    rev = "fab9efd8fad5c9a1b6a85816817f406c91702c56";
    sha256 = "0f6q0ws79k9mnm604jhrd5kihilgzzl7i4607nqqagf9xygphlv8" ;
  };
  buildInputs = [ coreutils libtool bison automake flex gettext texinfo zlib unzip help2man which ncurses ];

  nativeBuildInputs = [ makeWrapper autoconf ];

  preConfigure = "bash ./bootstrap";

}
