# verilator-4.228.nix

{ lib, stdenv, fetchFromGitHub
, perl, flex, bison, python3, autoconf
, which, cmake, help2man
, makeWrapper, glibcLocales
}:

stdenv.mkDerivation rec {
  pname = "verilator";
  version = "4.228";

  src = fetchFromGitHub {
    owner = pname;
    repo = pname;
    rev = "v${version}";
    sha256 = "1ahxwldlyxd0kandxia5dinxg33v08zbjpwfib65s11gqxvim1jf" ;
  };

  enableParallelBuilding = true;
  buildInputs = [ perl ];
  nativeBuildInputs = [ makeWrapper flex bison python3 autoconf help2man ];
  nativeCheckInputs = [ which ];

  doCheck = stdenv.isLinux; # darwin tests are broken for now...
  checkTarget = "test";

  preConfigure = "autoconf";

  postPatch = ''
    patchShebangs bin/* src/{flexfix,vlcovgen} test_regress/{driver.pl,t/*.pl}
  '';

  postInstall = lib.optionalString stdenv.isLinux ''
    for x in $(ls $out/bin/verilator*); do
      wrapProgram "$x" --set LOCALE_ARCHIVE "${glibcLocales}/lib/locale/locale-archive"
    done
  '';

  meta = with lib; {
    description = "Fast and robust (System)Verilog simulator/compiler";
    homepage    = "https://www.veripool.org/wiki/verilator";
    license     = with licenses; [ lgpl3Only artistic2 ];
    platforms   = platforms.unix;
    maintainers = with maintainers; [ thoughtpolice ];
  };
}
